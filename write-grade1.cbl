       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR. THANATHAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION.
       FILE SECTION.
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           05 STU-ID   PIC   X(8).
           05 MIDTERM-SCORE  PIC   9(2)V9(2).
           05 FINAL-SCORE  PIC   9(2)V9(2).
           05 PROJECT-SCORE  PIC   9(2)V9(2).
       PROCEDURE DIVISION.
       BEGIN.
           OPEN  OUTPUT SCORE-FILE
           MOVE "00000001" TO STU-ID 
           MOVE "32" TO MIDTERM-SCORE 
           MOVE "25" TO FINAL-SCORE
           MOVE "9" TO PROJECT-SCORE
           WRITE SCORE-DETAIL  
           

           MOVE "00000002" TO STU-ID 
           MOVE "18" TO MIDTERM-SCORE 
           MOVE "22" TO FINAL-SCORE
           MOVE "10" TO PROJECT-SCORE
           WRITE SCORE-DETAIL

           MOVE "00000003" TO STU-ID 
           MOVE "20" TO MIDTERM-SCORE 
           MOVE "38" TO FINAL-SCORE
           MOVE "15" TO PROJECT-SCORE
           WRITE SCORE-DETAIL

           MOVE "00000004" TO STU-ID 
           MOVE "18.5" TO MIDTERM-SCORE 
           MOVE "18" TO FINAL-SCORE
           MOVE "4" TO PROJECT-SCORE
           WRITE SCORE-DETAIL
           
           MOVE "00000005" TO STU-ID 
           MOVE "30" TO MIDTERM-SCORE 
           MOVE "20" TO FINAL-SCORE
           MOVE "10" TO PROJECT-SCORE
           WRITE SCORE-DETAIL
           CLOSE SCORE-FILE


           GOBACK 
           .